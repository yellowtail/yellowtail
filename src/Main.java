import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import com.yellowtail.Game;



public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int WIDTH = 800;
		int HEIGHT = 600;
		Game game = new Game();
		game.setMinimumSize(new Dimension(WIDTH - 10, HEIGHT - 10));
		game.setMaximumSize(new Dimension(WIDTH, HEIGHT));
		game.setPreferredSize(new Dimension(WIDTH - 10, HEIGHT - 10));
		JFrame frame = new JFrame(Game.NAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(game, BorderLayout.CENTER);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		game.start();
		System.exit(0);
	}

}

package com.yellowtail;

public class Command 
{
	public static Command Select = new Command(0);
	public static Command Up = new Command(1);
	public static Command Down = new Command(2);
	public static Command Left = new Command(3);
	public static Command Right = new Command(4);
	public static Command Inventory = new Command(5);
	public static Command Attack = new Command(6);
	public static Command ZoomOut = new Command(7);
	public static Command ZoomIn = new Command(8);
	public static Command Menu = new Command(9);
	public static Command[] CommandList = new Command[]{ 
		Command.Select, Command.Up, Command.Down, Command.Left, 
		Command.Right, Command.Inventory, Command.Attack, 
		Command.ZoomOut, Command.ZoomIn, Command.Menu };
	
	private boolean _active;
	private int _code;
	private long _activeStart;
	private int _delay = 175;
	
	public Command(int code)
	{
		this._code = code;
		this._active = false;
	}
	
	public int getCode()
	{
		return this._code;
	}
	
	public boolean isDelayedActive()
	{
		if(System.currentTimeMillis() > _activeStart + _delay)
		{
			if(_active)
			{
				_activeStart += _delay;
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}
	
	public boolean isActive()
	{
		return this._active;
	}
	 
	public void activate()
	{
		this._active = true;
		this._activeStart = System.currentTimeMillis() - _delay;
	}
	
	public void deactivate()
	{
		this._active = false;
		this._activeStart = 0;
	}
}

package com.yellowtail.level;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

import com.yellowtail.Command;
import com.yellowtail.Game;
import com.yellowtail.InputHandler;
import com.yellowtail.Level;
import com.yellowtail.Sound;
import com.yellowtail.events.CommandEventHandler;
import com.yellowtail.menu.Menu;
import com.yellowtail.screen.BasicScreen;
import com.yellowtail.screen.Layer;
import com.yellowtail.screen.Screen;
import com.yellowtail.screen.Style;

public class MainMenu implements Level, CommandEventHandler
{
	private Game _game;
	private Screen _screen;
	private Menu _menu;
	private boolean _loaded = false;
	
	public MainMenu(Game game)
	{
		this._game = game;
	}
	
	@Override
	public void drawFrame(Graphics g, Point size) 
	{
		_screen.drawFrame(g, size);
	}

	@Override
	public void load() 
	{
		this._screen = new BasicScreen(Color.gray);
		this._menu = new Menu("MainMenu", "YellowTail", new Point(), 
				new Style(new Font("Console",Font.BOLD,40), Color.yellow, Color.gray), 
				new Style(new Font("Console", Font.BOLD, 18), Color.red, Color.gray), 
				new Style(new Font("Console", Font.PLAIN, 18), Color.white, Color.gray), 
				Layer.Top);
		_menu.AddMenuItem("Load Game");
		_menu.AddMenuItem("New Game");
		_menu.AddMenuItem("Multiplayer");
		_menu.AddMenuItem("Options");
		_menu.AddMenuItem("Credits");
		this._menu.setPosition(new Point(_game.getWidth()/2-_menu.getWidth()/2, _game.getHeight()/2 - _menu.getHeight()));
		this._screen.getGUI().addElement(_menu);
		Sound.title_music.playContinously();
		InputHandler.inputWatcher.addCommandListener(this);
		this._loaded = true;
	}

	@Override
	public void unload() 
	{
		InputHandler.inputWatcher.removeCommandListener(this);
		this._loaded = false;
		this._screen = null;
		this._menu = null;
		Sound.title_music.playEnd();
	}

	@Override
	public void CommandEvent(Command cmd) 
	{
		if(_loaded)
		{
			if(cmd == Command.Select)
			{
				if(cmd.isDelayedActive())
				{
					switch(_menu.getSelected().getText())
					{
					case "Load Game":
						_game.Load("World");
						break;
					case "New Game":
						break;
					case "Multiplayer":
						break;
					case "Options":
						break;
					case "Credits":
						break;
					default:
						break;
					}
				}
			}
			else if(cmd == Command.Up)
			{
				if(cmd.isDelayedActive())
				{
					_menu.SelectPrev();
				}
			}
			else if(cmd == Command.Down)
			{
				if(cmd.isDelayedActive())
				{
					_menu.SelectNext();
				}
			}
		}
	}

}

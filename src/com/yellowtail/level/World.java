package com.yellowtail.level;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

import com.yellowtail.Command;
import com.yellowtail.Game;
import com.yellowtail.InputHandler;
import com.yellowtail.Level;
import com.yellowtail.entities.Entities;
import com.yellowtail.entities.EntityController;
import com.yellowtail.map.WorldMap;
import com.yellowtail.menu.Menu;
import com.yellowtail.screen.BasicGUI;
import com.yellowtail.screen.GUI;
import com.yellowtail.screen.Layer;
import com.yellowtail.screen.Style;

public class World implements Level
{
	private GUI _gui;
	private com.yellowtail.Map _map;
	private Game _game;
	private Menu _inGameMenu;
	private EntityController _entityController;
	private boolean _loaded;
	
	public World(Game game)
	{
		this._game = game;
	}
	
	@Override
	public void drawFrame(Graphics g, Point size) 
	{
		_entityController.update();
		_entityController.getActiveCamera().DrawFrame(g, size);
		_gui.overlay(g, size);
	}

	@Override
	public void CommandEvent(Command cmd)
	{
		if(_loaded)
		{
			if(Command.Up == cmd)
			{
				if(_inGameMenu != null && cmd.isDelayedActive()){ _inGameMenu.SelectPrev(); }
			}
			if(Command.Down == cmd)
			{
				if(_inGameMenu != null && cmd.isDelayedActive()){ _inGameMenu.SelectNext(); }
			}
			if(Command.Select == cmd)
			{
				if(_inGameMenu != null && cmd.isDelayedActive())
				{
					switch (_inGameMenu.getSelected().getText()) 
					{
					case "Continue":
						closeMenu();
						break;
					case "Save":
						break;
					case "Quit to Main Menu":
						closeMenu();
						_game.Load("MainMenu");
						break;
					default:
						break;
					}
				}
			}
			if(Command.Menu == cmd)
			{
				if(_inGameMenu == null && cmd.isDelayedActive())
				{
					showMenu();
				}
				else if(cmd.isDelayedActive())
				{
					closeMenu();
				}
			}
		}
	}

	@Override
	public void load() 
	{
		this._map = new WorldMap();
		this._entityController = new EntityController(_map);
		this._entityController.spawnEntity(Entities.PlayerChar, new Point(0,0));
		this._gui = new BasicGUI();
		this._loaded = true;
		this._entityController.enableInput();
		InputHandler.inputWatcher.addCommandListener(this);
	}

	@Override
	public void unload() 
	{
		InputHandler.inputWatcher.removeCommandListener(this);
		this._loaded = false;
		this._map = null;
		this._entityController = null;
		this._gui = null;
	}
	
	private void showMenu()
	{
		this._entityController.disableInput();
		_inGameMenu = new Menu("GameMenu", "Pause Menu", new Point(), 
			new Style(new Font("Console",Font.BOLD,40), Color.yellow, Color.gray), 
			new Style(new Font("Console", Font.BOLD, 18),Color.red, Color.gray), 
			new Style(new Font("Console",Font.BOLD,18), Color.white, Color.gray),
			Layer.Top);
		_inGameMenu.AddMenuItem("Continue");
		_inGameMenu.AddMenuItem("Save");
		_inGameMenu.AddMenuItem("Quit to Main Menu");
		_inGameMenu.setPosition(new Point(_game.getWidth()/2-_inGameMenu.getWidth()/2,_game.getHeight()/2-_inGameMenu.getHeight()));
		_gui.addElement(_inGameMenu);
	}
	
	private void closeMenu()
	{
		_gui.removeElement(_inGameMenu.getLayer(),_inGameMenu.getName());
		_inGameMenu = null;
		this._entityController.enableInput();
	}

}

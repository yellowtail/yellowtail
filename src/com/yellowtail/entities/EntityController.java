package com.yellowtail.entities;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.yellowtail.Map;
import com.yellowtail.screen.Camera;

public class EntityController
{
	private HashMap<Integer, Entity> _activeEntities;
	private int _activeIDs = 0;
	private Map _map;
	private Entity _activeEntity;
	private boolean _inputStatus = false;
	
	public EntityController(Map map)
	{
		_activeEntities = new HashMap<Integer,Entity>();
		this._map = map;
	}
	
	public int spawnEntity(Entities entity, Point position)
	{
		switch (entity) {
		case FreeCamera:
			_activeEntities.put(_activeIDs, new FreeCameraEntity(_activeIDs,position));
			break;
		case PlayerChar:
			_activeEntities.put(_activeIDs, new PlayerEntity(this, _activeIDs,position));				
		default:
			break;
		}
		if(_activeEntities.size()>_activeIDs)
		{
			if(_inputStatus){ _activeEntities.get(_activeIDs).enableInput(); }
			if(_activeEntity == null && _activeEntities.size() > 0){ _activeEntity = _activeEntities.get(_activeIDs); }
			_activeIDs++; 
			return (_activeIDs - 1);
		}
		else 
		{
			return -1;
		}
	}
	
	public void removeEntity(Entity entity)
	{
		if(_activeEntity.getActiveID() == entity.getActiveID())
		{
			if(_activeEntities.size() <= 1){ _activeEntity = null; }
			else{ _activeEntity = _activeEntities.get(0); }
		}
		_activeEntities.remove(entity.getActiveID());
	}
	
	public ArrayList<Entity> getEntitiesInRange(Point centerPosition, int range)
	{
		ArrayList<Entity> res = new ArrayList<>();
		Entity tmp;
		Point lowPoint = new Point(centerPosition.x - range / 2, centerPosition.y - range / 2);
		Point highPoint = new Point(centerPosition.x + range / 2, centerPosition.y + range / 2);
		for (Iterator<Entity> iterator = _activeEntities.values().iterator(); iterator.hasNext();) {
			tmp = (Entity) iterator.next();
			if(tmp.getPosition().x >= lowPoint.x && tmp.getPosition().x <= highPoint.x && tmp.getPosition().y >= lowPoint.y && tmp.getPosition().y <= highPoint.y)
			{
				res.add(tmp);
			}
		}
		return res;
	}
	
	public void setActiveEntity(int activeID)
	{
		if(_activeEntities.containsKey(activeID))
		{
			if(_activeEntity != null){ _activeEntity.disableInput(); }
			_activeEntity = _activeEntities.get(activeID); 
			_activeEntity.enableInput(); 
		}
	}
	
	public Camera getActiveCamera()
	{
		if(_activeEntity != null)
		{
			return _activeEntity.getCamera();
		}
		else 
		{
			return null;
		}
	}
	
	public void update() 
	{
		if(getActiveCamera() != null)
		{
			ArrayList<Entity> entities = getEntitiesInRange(_activeEntity.getCamera().getWorldPosition(), _activeEntity.getCamera().getViewRange());
			for(int i=0; i<entities.size();i++)
			{
				entities.get(i).update();
			}
			getActiveCamera().UpdateEntityTiles(entities);
		}
	}
	
	public Entity getActiveEntity()
	{
		return _activeEntity;
	}
	
	public void disableInput()
	{
		if(getActiveEntity() != null){ getActiveEntity().disableInput(); }
	}
	
	public void enableInput()
	{
		if(getActiveEntity() != null){ getActiveEntity().enableInput(); }
	}
	
	public Map getMap()
	{
		return _map;
	}
}

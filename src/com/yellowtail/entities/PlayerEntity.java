package com.yellowtail.entities;

import java.awt.Point;
import com.yellowtail.Command;
import com.yellowtail.Sound;
import com.yellowtail.utils.Animation;
import com.yellowtail.world.Direction;
import com.yellowtail.world.MoveType;
import com.yellowtail.world.Sprite;

public class PlayerEntity extends Entity
{
	private double _speed = 1;
	private int _distance = 240;
	private int _walkAnimationDelay = 120;
	private Point _tileResolution = new Point(240,480);
	
	public PlayerEntity(EntityController parent,int activeID, Point position)
	{
		setParent(parent);
		_entityType = Entities.PlayerChar;
		this._activeID = activeID;
		this._position = position;
		setTileResolution(_tileResolution);
		setDefaultSprite(Sprite.FMainFront);
		MoveParameters moveParms = new MoveParameters(_speed,_distance,Sound.player_step);
		moveParms.setAnimation(Direction.North, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainBack,Sprite.FMainBackRH, Sprite.FMainBack,Sprite.FMainBackLH}));
		moveParms.setAnimation(Direction.South, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainFront,Sprite.FMainFrontRH, Sprite.FMainFront,Sprite.FMainFrontLH}));
		moveParms.setAnimation(Direction.West, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainLSide,Sprite.FMainLSideRH, Sprite.FMainLSide,Sprite.FMainLSideLH}));
		moveParms.setAnimation(Direction.East, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainRSide,Sprite.FMainRSideRH, Sprite.FMainRSide,Sprite.FMainRSideLH}));
		addMovementParmeters(MoveType.Walk, moveParms);
		moveParms = new MoveParameters(_speed,_distance,Sound.player_step);
		moveParms.setAnimation(Direction.North, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainBack,Sprite.FMainBackRH, Sprite.FMainBack,Sprite.FMainBackLH}));
		moveParms.setAnimation(Direction.South, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainFront,Sprite.FMainFrontRH, Sprite.FMainFront,Sprite.FMainFrontLH}));
		moveParms.setAnimation(Direction.West, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainLSide,Sprite.FMainLSideRH, Sprite.FMainLSide,Sprite.FMainLSideLH}));
		moveParms.setAnimation(Direction.East, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainRSide,Sprite.FMainRSideRH, Sprite.FMainRSide,Sprite.FMainRSideLH}));
		addMovementParmeters(MoveType.Run, moveParms);
		moveParms = new MoveParameters(_speed,_distance,Sound.player_swim);
		moveParms.setAnimation(Direction.North, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainBack,Sprite.FMainBackRH, Sprite.FMainBack,Sprite.FMainBackLH}));
		moveParms.setAnimation(Direction.South, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainFront,Sprite.FMainFrontRH, Sprite.FMainFront,Sprite.FMainFrontLH}));
		moveParms.setAnimation(Direction.West, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainLSide,Sprite.FMainLSideRH, Sprite.FMainLSide,Sprite.FMainLSideLH}));
		moveParms.setAnimation(Direction.East, new Animation(_walkAnimationDelay, new Sprite[] {Sprite.FMainRSide,Sprite.FMainRSideRH, Sprite.FMainRSide,Sprite.FMainRSideLH}));
		addMovementParmeters(MoveType.Swim, moveParms);
	}
	
	public void CommandEvent(Command cmd) 
	{
		if(Command.Up == cmd)
		{
			move(Direction.North);
		}
		if(Command.Down == cmd)
		{
			move(Direction.South);
		}
		if(Command.Left == cmd)
		{
			move(Direction.West);
		}
		if(Command.Right == cmd)
		{
			move(Direction.East);
		}
		if(Command.ZoomIn == cmd)
		{
			this._camera.SpanIn();
		}
		if(Command.ZoomOut == cmd)
		{
			this._camera.SpanOut();
		}
	}
}

package com.yellowtail.entities;

import java.util.HashMap;
import java.util.Map;

import com.yellowtail.Sound;
import com.yellowtail.utils.Animation;
import com.yellowtail.world.Direction;

public class MoveParameters 
{
	private Map<Direction, Animation> _animations;
	private int _distance;
	private double _speed;
	private Sound _sound;
	
	public MoveParameters(Map<Direction, Animation> animations, double speed, int distance, Sound sound) 
	{
		_animations = animations;
		_speed = speed;
		_distance = distance;
		_sound = sound;
	}
	
	public MoveParameters(double speed, int distance, Sound sound)
	{
		_speed = speed;
		_distance = distance;
		_sound = sound;
		_animations = new HashMap<Direction, Animation>();
	}
	
	public MoveParameters()
	{
		_animations = new HashMap<Direction, Animation>();
	}
	
	public Animation getAnimation(Direction direction)
	{
		return _animations.get(direction);
	}
	
	public void setAnimation(Direction direction, Animation animation)
	{
		_animations.put(direction, animation);
	}
	
	public double getSpeed()
	{
		return _speed;
	}
	
	public void setSpeed(double speed)
	{
		_speed = speed;
	}
	
	public Sound getSound()
	{
		return _sound;
	}
	
	public void setSound(Sound sound)
	{
		_sound = sound;
	}
	
	public int getDistance()
	{
		return _distance;
	}
	
	public void setDistance(int distance)
	{
		_distance = distance;
	}
}

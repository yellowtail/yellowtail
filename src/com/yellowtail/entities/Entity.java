package com.yellowtail.entities;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.yellowtail.InputHandler;
import com.yellowtail.Time;
import com.yellowtail.events.CommandEventHandler;
import com.yellowtail.map.TileCode;
import com.yellowtail.screen.TileCamera;
import com.yellowtail.world.Direction;
import com.yellowtail.world.MoveType;
import com.yellowtail.world.RenderSetRequest;
import com.yellowtail.world.Sprite;

public abstract class Entity implements CommandEventHandler
{
	protected Entities _entityType;
	protected String _name;
	protected int _activeID;
	protected Point _position;
	protected TileCamera _camera;
	private Direction _direction;
	private boolean _move;
	private int _traveled;
	private Point _tileResolution;
	private Sprite _currentSprite;
	private boolean _keepGoing;
	private EntityController _parent;
	private Map<MoveType, MoveParameters> _moveParms;
	private MoveType _activeMoveParms;

	public Entities getEntityType()
	{
		return _entityType;
	}
	
	public int getActiveID() 
	{
		return _activeID;
	}

	public void setActiveID(int activeID) 
	{
		_activeID = activeID;
	}

	public String getName() 
	{
		return _name;
	}

	public Point getPosition() 
	{
		return _position;
	}

	public void setPosition(Point position) 
	{
		this._position = position;
	}

	public TileCamera getCamera() 
	{
		if(_camera != null)
		{
			return _camera;
		}
		else 
		{
			_camera = new TileCamera(_parent.getMap());
			return _camera;
		}
	}

	public void enableInput()
	{
		InputHandler.inputWatcher.addCommandListener(this);
	}
	
	public void disableInput()
	{
		InputHandler.inputWatcher.removeCommandListener(this);
	}

	public TileCode getTileCode()
	{
		return new TileCode(_position, _tileResolution, _currentSprite);
	}
	
	public void update() 
	{
		if(_position == null){ _position = new Point(); }
		if(_tileResolution == null){ _tileResolution = new Point(); }
		if(_moveParms == null){ _moveParms = new HashMap<MoveType, MoveParameters>(); }
		ArrayList<TileCode> centerTiles = getParent().getMap().getRenderSet(new RenderSetRequest(_position, 0)).getTiles();
		TileCode centerTile = centerTiles.get(3);
		if(centerTile.getSprite() == Sprite.SolidWater)
		{
			if(_activeMoveParms != MoveType.Swim)
			{
				_moveParms.get(_activeMoveParms).getSound().playEnd();
				if(_direction != null){_moveParms.get(_activeMoveParms).getAnimation(_direction).stop();}
				_activeMoveParms = MoveType.Swim;
				if(_direction != null){_moveParms.get(_activeMoveParms).getAnimation(_direction).start();}
				if(_move){_moveParms.get(_activeMoveParms).getSound().playContinously();}
			}
		}
		else
		{
			if(_activeMoveParms != MoveType.Walk)
			{
				_moveParms.get(_activeMoveParms).getSound().playEnd();
				if(_direction != null){_moveParms.get(_activeMoveParms).getAnimation(_direction).stop();}
				_activeMoveParms = MoveType.Walk;
				if(_direction != null){_moveParms.get(_activeMoveParms).getAnimation(_direction).start();}
				if(_move){_moveParms.get(_activeMoveParms).getSound().playContinously();}
			}
		}
		if(_moveParms.containsKey(_activeMoveParms))
		{ 
			if(_moveParms.get(_activeMoveParms).getAnimation(_direction) != null)
			{
				_currentSprite = _moveParms.get(_activeMoveParms).getAnimation(_direction).getSprite();
				if(_move)
				{
					switch(_direction)
					{
					case North:
						_position.y -= (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						_traveled += (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						break;
					case South:
						_position.y += (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						_traveled += (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						break;
					case West:
						_position.x -=  (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						_traveled += (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						break;
					case East:
						_position.x +=  (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						_traveled += (int)(_moveParms.get(_activeMoveParms).getSpeed() * (double)Time.GameTime.getSinceLastFrameStart());
						break;
					default:
						break;
					}
					if(_traveled >= _moveParms.get(_activeMoveParms).getDistance())
					{
						_traveled = 0;
						if(!_keepGoing)
						{
							if(_moveParms.get(_activeMoveParms).getSound() != null){ _moveParms.get(_activeMoveParms).getSound().playEnd(); }
							_move = false;
							_moveParms.get(_activeMoveParms).getAnimation(_direction).stop();
						}
						else 
						{
							_keepGoing = false;
						}
					}
					else
					{
						_keepGoing = false;
					}
				}
			}
		}
		centerCamera();
	}

	public void move(Direction direction)
	{
		if(!_move)
		{
			_move = true;
			_direction = direction;
			_moveParms.get(_activeMoveParms).getAnimation(_direction).start();
			if(_moveParms.get(_activeMoveParms).getSound() != null){ _moveParms.get(_activeMoveParms).getSound().playContinously(); }
		}
		else if(_direction == direction)
		{
			_keepGoing = true;
		}
	}
		
	protected void setDefaultSprite(Sprite sprite)
	{
		_currentSprite = sprite;
	}
	
	protected void setTileResolution(Point tileResolution)
	{
		_tileResolution = tileResolution;
	}
	
	protected void centerCamera()
	{
		if(_camera != null)
		{
			_camera.SetWorldPosition(_position);
		}
		else 
		{
			_camera = new TileCamera(_parent.getMap());
			_camera.SetWorldPosition(_position);
		}
	}
	
	protected EntityController getParent()
	{
		return this._parent;
	}
	
	protected void setParent(EntityController parent)
	{
		this._parent = parent;
	}
	
	protected void addMovementParmeters(MoveType moveType, MoveParameters moveParm)
	{
		if(_moveParms == null){ _moveParms = new HashMap<MoveType, MoveParameters>(); }
		_moveParms.put(moveType, moveParm);
		if(_activeMoveParms == null){ _activeMoveParms = moveType; }
	}
	
	protected void setActiveMoveParameters(MoveType moveType)
	{
		_activeMoveParms = moveType;
	}
}

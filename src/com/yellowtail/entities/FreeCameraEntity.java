package com.yellowtail.entities;

import java.awt.Point;

import com.yellowtail.Command;
import com.yellowtail.map.TileCode;

public class FreeCameraEntity extends Entity
{
	
	public FreeCameraEntity(int activeID, Point position)
	{
		_entityType = Entities.FreeCamera;
		this._activeID = activeID;
		this._position = position;
	}

	@Override
	public TileCode getTileCode() 
	{
		return null;
	}

	public void CommandEvent(Command cmd) 
	{
		if(Command.Up == cmd)
		{
			this._position.y--;
			this._camera.SlewUp();
		}
		if(Command.Down == cmd)
		{
			this._position.y++;
			this._camera.SlewDown();
		}
		if(Command.Left == cmd)
		{
			this._position.x--;
			this._camera.SlewLeft();
		}
		if(Command.Right == cmd)
		{
			this._position.x++;
			this._camera.SlewRight();
		}
		if(Command.ZoomIn == cmd)
		{
			this._camera.SpanIn();
		}
		if(Command.ZoomOut == cmd)
		{
			this._camera.SpanOut();
		}
	}
}

package com.yellowtail;

public class Time 
{
	public static Time GameTime = new Time();
	
	private long _startTime;
	private int _lastTime;
	
	public int getLastFrameTime()
	{
		return _lastTime;
	}
	
	public void setStartFrame()
	{
		this._startTime = System.currentTimeMillis();
	}
	
	public void setStopFrame()
	{
		this._lastTime = (int) (System.currentTimeMillis() - _startTime);
	}
	
	public int getSinceFrameStart()
	{
		return (int) (System.currentTimeMillis() - this._startTime);
	}
	
	public int getSinceLastFrameStart()
	{
		return getSinceFrameStart() + getLastFrameTime();
	}
}

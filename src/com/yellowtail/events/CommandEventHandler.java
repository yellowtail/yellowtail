package com.yellowtail.events;

import com.yellowtail.Command;

public interface CommandEventHandler
{
	public void CommandEvent(Command cmd);
}

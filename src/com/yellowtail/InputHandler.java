package com.yellowtail;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.yellowtail.events.CommandEventHandler;

public class InputHandler implements KeyListener {

	public static InputHandler inputWatcher = new InputHandler();
	
	private Map<Integer, ArrayList<Command>> _keyCodeMatrix;
	private ArrayList<CommandEventHandler> _listeners;
	
	public InputHandler()
	{
		this._keyCodeMatrix = new HashMap<Integer, ArrayList<Command>>();
		this._listeners = new ArrayList<>();
	}
	
	public void attachKeyListener(Component component)
	{
		component.addKeyListener(this);
	}
	
	public void processCommands()
	{
		for(int i=0;i<Command.CommandList.length;i++)
		{
			boolean active = Command.CommandList[i].isActive();
			@SuppressWarnings("unchecked")
			ArrayList<CommandEventHandler> listeners = (ArrayList<CommandEventHandler>) _listeners.clone(); 
			for(int a=0;a<listeners.size();a++)
			{
				if(active && _listeners.contains(listeners.get(a)))
				{
					listeners.get(a).CommandEvent(Command.CommandList[i]);
					//System.out.println(listeners.get(a).toString()+" "+Command.CommandList[i].getCode());
				}
			}
		}
	}
	
	public void SetCommand(Command cmd,int keyCode)
	{
		if(!this._keyCodeMatrix.containsKey(keyCode))
		{
			this._keyCodeMatrix.put(keyCode, new ArrayList<Command>());
		}
		this._keyCodeMatrix.get(keyCode).add(cmd);
	}
	
	@Override
	public void keyPressed(KeyEvent key) 
	{
		if(this._keyCodeMatrix.containsKey(key.getKeyCode()))
		{
			for(int i =0; i<this._keyCodeMatrix.get(key.getKeyCode()).size();i++)
			{
				if(!this._keyCodeMatrix.get(key.getKeyCode()).get(i).isActive())
				{
					this._keyCodeMatrix.get(key.getKeyCode()).get(i).activate();
				}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent key) 
	{
		if(this._keyCodeMatrix.containsKey(key.getKeyCode()))
		{
			for(int i =0; i<this._keyCodeMatrix.get(key.getKeyCode()).size();i++)
			{
				if(this._keyCodeMatrix.get(key.getKeyCode()).get(i).isActive())
				{
					this._keyCodeMatrix.get(key.getKeyCode()).get(i).deactivate();
				}
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) 
	{
		
	}

	public void addCommandListener(CommandEventHandler listener)
	{
		_listeners.add(listener);
	}
	
	public void removeCommandListener(CommandEventHandler listener)
	{
		_listeners.remove(listener);
	}
}

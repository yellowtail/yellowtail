package com.yellowtail;

import java.awt.Point;
import com.yellowtail.world.RenderSet;
import com.yellowtail.world.RenderSetRequest;

public interface Map 
{
	public Point getTileResolution();
	public RenderSet getRenderSet(RenderSetRequest request);
}

package com.yellowtail.utils;

import com.yellowtail.world.Sprite;

public class Animation
{
	
	private int _delay;
	private Sprite[] _sprites;
	private long _startTime;
	
	public Animation(int delay, Sprite[] sprites)
	{
		_delay = delay;
		_sprites = sprites;
	}
	
	public void start()
	{
		if(_startTime == 0)
		{
			_startTime = System.currentTimeMillis();
		}
	}
	
	public Sprite getSprite() 
	{
		if(_startTime > 0)
		{
			int timeLapsed = (int) (System.currentTimeMillis() - _startTime);
			if(timeLapsed > _delay * _sprites.length)
			{
				_startTime += (int)Math.floor(timeLapsed / _delay) * _delay;
				return _sprites[0];
			} 
			else 
			{
				if((int)Math.floor(timeLapsed / _delay) < _sprites.length)
				{
					return _sprites[(int)Math.floor(timeLapsed / _delay)];
				}
				else 
				{
					return _sprites[0];
				}
			}
		}
		else 
		{
			return _sprites[0];
		}
	}

	public void stop() {
		_startTime = 0;
	}

}

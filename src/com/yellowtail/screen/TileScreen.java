package com.yellowtail.screen;

import java.awt.Graphics;
import java.awt.Point;


public class TileScreen implements Screen 
{
	private GUI _gui;
	private TileCamera _camera;

	public TileScreen(com.yellowtail.Map map)
	{
		this._gui = new BasicGUI();
		this._camera = new TileCamera(map);
	}
	
	public void setCameraPosition(Point position)
	{
		_camera.SetWorldPosition(position);
	}
	
	public void SlewUp()
	{
		_camera.SlewUp();
	}
	
	public void SlewDown()
	{
		_camera.SlewDown();
	}
	
	public void SlewRight()
	{
		_camera.SlewRight();
	}
	
	public void SlewLeft()
	{
		_camera.SlewLeft();
	}
	
	public void SpanIn()
	{
		this._camera.SpanIn();
	}
	
	public void SpanOut()
	{
		this._camera.SpanOut();
	}
	
	@Override
	public void drawFrame(Graphics g, Point size) 
	{
		this._camera.DrawFrame(g, size);
		this._gui.overlay(g, size);
	}

	@Override
	public GUI getGUI() 
	{
		return this._gui;
	}

	@Override
	public Camera getCamera() 
	{
		return this._camera;
	}

}

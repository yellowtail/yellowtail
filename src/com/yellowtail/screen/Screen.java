package com.yellowtail.screen;

import java.awt.Graphics;
import java.awt.Point;

public interface Screen 
{
	public GUI getGUI();
	public Camera getCamera();
	public void drawFrame(Graphics g, Point size);
}

package com.yellowtail.screen;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import com.yellowtail.entities.Entity;

public class SolidCamera implements Camera 
{
	private Color _color;
	
	public SolidCamera(Color color)
	{
		this._color = color;
	}
	@Override
	public void DrawFrame(Graphics g, Point size) {
		// Generate main menu background
		g.setColor(_color);
		g.fillRect(0, 0, size.x, size.y);
	}

	@Override
	public void UpdateEntityTiles(
			ArrayList<Entity> entities) {
		// TODO Auto-generated method stub
		
	}

}

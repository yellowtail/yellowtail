package com.yellowtail.screen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


public class BasicScreen implements Screen
{
	private Camera _camera;
	private GUI _gui;
	
	public BasicScreen(Color color)
	{
		this._camera = new SolidCamera(color);
		this._gui = new BasicGUI();
	}
	
	public BasicScreen(GUI gui, Camera camera)
	{
		this._camera = camera;
		this._gui = gui;
	}

	@Override
	public Camera getCamera()
	{
		return _camera;
	}
	
	@Override
	public GUI getGUI()
	{
		return _gui;
	}
	
	@Override
	public void drawFrame(Graphics g, Point size)
	{
		_camera.DrawFrame(g, size);
		_gui.overlay(g, size);
	}

}

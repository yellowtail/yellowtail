package com.yellowtail.screen;

import java.awt.Graphics;
import java.awt.Point;

public interface GUIElement 
{
	public String getName();
	public Layer getLayer();
	public void setPosition(Point position);
	public void overlay(Graphics g, Point size);
	//An interface for specific GUI elements that can generate an image.
}

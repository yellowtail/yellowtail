package com.yellowtail.screen;

import java.awt.Color;
import java.awt.Font;

public class Style 
{
	private Font _font;
	private Color _color;
	private Color _bgColor;
	
	public Style(Font font, Color color, Color bgColor)
	{
		this._font = font;
		this._color = color;
		this._bgColor = bgColor;
	}
	
	public Font getFont()
	{
		return _font;
	}
	
	public Color getColor()
	{
		return _color;
	}
	
	public Color getBGColor()
	{
		return this._bgColor;
	}
	
	public void setFont(Font font)
	{
		this._font = font;
	}
	
	public void setColor(Color color)
	{
		this._color = color;
	}

	public void setBGColor(Color color)
	{
		this._bgColor = color;
	}
	
}

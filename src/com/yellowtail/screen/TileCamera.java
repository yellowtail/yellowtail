/*
 * TODO: Change _maxSpan to 250, temporarily changed to 2500 for map testing
 */

package com.yellowtail.screen;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import com.yellowtail.entities.Entity;
import com.yellowtail.world.RenderSet;
import com.yellowtail.world.RenderSetRequest;

public class TileCamera implements Camera 
{
	private com.yellowtail.Map _map;
	private Point _position;
	private int _range;
	private RenderSet _renderSet;
	private int _minSpan;
	private int _maxSpan;
	private ArrayList<Entity> _entities;
	private HashMap<Integer,Image> _scaledSprites;
	
	public TileCamera(com.yellowtail.Map map)
	{
		this._map = map;
		this._position = new Point(0,0);
		this._range = 5000;
		this._minSpan = 1000;
		this._maxSpan = 7000;
		this._entities = new ArrayList<Entity>();
		this._scaledSprites = new HashMap<Integer,Image>();
	}
	
	public Point getWorldPosition()
	{
		return this._position;
	}
	
	public int getViewRange()
	{
		return this._range;
	}
	
	public void SetWorldPosition(Point position)
	{
		this._position = (Point) position.clone();
	}
	
	public void SlewUp()
	{
		this._position.y--;
	}
	
	public void SlewDown()
	{
		this._position.y++;
	}
	
	public void SlewLeft()
	{
		this._position.x--;
	}
	
	public void SlewRight()
	{
		this._position.x++;
	}
	
	@Override
	public void UpdateEntityTiles(ArrayList<Entity> entities) 
	{
		this._entities = entities;
	}
	@Override
	public void DrawFrame(Graphics g, Point size) 
	{
		Point currentPosition = (Point) this._position.clone();
		double currentRange = _range;
		_renderSet = this._map.getRenderSet(new RenderSetRequest(currentPosition,(int)currentRange));
		for(int i=0;i<_entities.size();i++)
		{
			_renderSet.getTiles().add(_entities.get(i).getTileCode());
		}
		Point2D scale = new Point2D.Double((double)size.x/currentRange,(double)size.y/(currentRange*((double)size.y/(double)size.x)));
		for (int i=0;i<_renderSet.getTiles().size();i++) 
		{
			Point2D coords = new Point2D.Double(scale.getX()*(double)_renderSet.getTiles().get(i).getCoordinates().x,scale.getY()*(double)_renderSet.getTiles().get(i).getCoordinates().y);
			Point2D offset = new Point2D.Double((double)size.x/2-scale.getX()*(double)currentPosition.x,(double)size.y/2-scale.getY()*(double)currentPosition.y);
			int tileWidth = (int)Math.ceil(scale.getX()*_renderSet.getTiles().get(i).getTileResolution().getX())+1;
			int tileHeight = (int)Math.ceil(scale.getY()*_renderSet.getTiles().get(i).getTileResolution().getY())+1;
			if(!_scaledSprites.containsKey(_renderSet.getTiles().get(i).getSprite().getID()))
			{
				_scaledSprites.put(_renderSet.getTiles().get(i).getSprite().getID(),_renderSet.getTiles().get(i).getSprite().getImage().getScaledInstance(tileWidth, tileHeight, Image.SCALE_SMOOTH));
			}
			g.drawImage(
						_scaledSprites.get(_renderSet.getTiles().get(i).getSprite().getID()), 
						(int)Math.round(coords.getX() + offset.getX() - tileWidth/2), 
						(int)Math.round(coords.getY() + offset.getY() - tileHeight/2), 
						tileWidth, 
						tileHeight, 
						null);
		}
	}
	
	public void SpanIn()
	{
		if (_range >= _minSpan)
		{ 
			//Sets minimum Zoom
			this._range -= 20;
			this._scaledSprites.clear();
		}
	}
	
	public void SpanOut()
	{
		if (_range <= _maxSpan)
		{
			//Sets maximum Zoom
			this._range += 20;
			this._scaledSprites.clear();
		}
	}
}
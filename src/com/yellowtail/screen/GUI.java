package com.yellowtail.screen;

import java.awt.Graphics;
import java.awt.Point;



public interface GUI 
{
	public void addElement(GUIElement element);
	public void addElement(GUIElement[] elements);
	public void removeElement(Layer layer, String name);
	public GUIElement getElement(Layer layer, String name);
	public void purge();
	public void overlay(Graphics g, Point size);	
}

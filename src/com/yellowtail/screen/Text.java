package com.yellowtail.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;


public class Text implements GUIElement 
{
	private String _name;
	private String _text;
	private Style _style;
	private Point _position;
	private Layer _layer;
	
	public Text(String name, Point position, String text, Style style, Layer layer)
	{
		this._name = name;
		this._position = position;
		this._text = text;
		this._style = style;
		this._layer = layer;
	}
	
	public Style getStyle()
	{
		return this._style;
	}
	
	public String getText()
	{
		return _text;
	}
	
	@Override
	public void setPosition(Point position) 
	{
		this._position = position;
	}
	
	public void SetColor(Color color)
	{
		this._style.setColor(color);
	}
	
	public void SetFont(Font font)
	{
		this._style.setFont(font);
	}

	public void SetText(String text)
	{
		this._text = text;
	}
	
	@Override
	public void overlay(Graphics g, Point size) 
	{
		g.setColor(_style.getColor());
		g.setFont(_style.getFont());
		g.drawString(_text, _position.x, _position.y);
	}

	@Override
	public Layer getLayer() 
	{
		return this._layer;
	}

	@Override
	public String getName() 
	{
		return this._name;
	}

}

package com.yellowtail.screen;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import com.yellowtail.entities.Entity;

public interface Camera 
{
	
	public void DrawFrame(Graphics g, Point size);
	public void UpdateEntityTiles(ArrayList<Entity> entities);
}

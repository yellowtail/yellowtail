package com.yellowtail.screen;

import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class BasicGUI implements GUI 
{

	private Map<Layer, Map<String, GUIElement>> _guiElements;
	
	public BasicGUI()
	{
		this._guiElements = new HashMap<Layer, Map<String,GUIElement>>();
		for(int i=0;i<Layer.values().length;i++)
		{
			this._guiElements.put(Layer.values()[i], new HashMap<String,GUIElement>());
		}
	}
	
	@Override
	public void addElement(GUIElement element)
	{
		this._guiElements.get(element.getLayer()).put(element.getName(), element);
	}
	
	@Override
	public void addElement(GUIElement[] elements)
	{
		for (int i = 0; i < elements.length; i++) 
		{
			this.addElement(elements[i]);
		}
	}
	
	@Override
	public void purge()
	{
		this._guiElements.clear();
		for(int i=0;i<Layer.values().length;i++)
		{
			this._guiElements.put(Layer.values()[i], new HashMap<String,GUIElement>());
		}
	}
	
	@Override
	public GUIElement getElement(Layer layer, String name)
	{
		return this._guiElements.get(layer).get(name);
	}
	
	@Override
	public void overlay(Graphics g, Point size) 
	{
		for(int i = 0; i < Layer.values().length; i++)
		{
				for (Entry<String, GUIElement> element : _guiElements.get(Layer.values()[i]).entrySet()) 
				{
					element.getValue().overlay(g, size);
				}
		}
		
	}

	@Override
	public void removeElement(Layer layer, String name) 
	{
		this._guiElements.get(layer).remove(name);
	}

}

package com.yellowtail;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	public static final Sound title_music = new Sound("/com/yellowtail/resources/title_music.wav");
	public static final Sound day_music = new Sound("/com/yellowtail/resources/day_music.wav");
	public static final Sound night_music = new Sound("/com/yellowtail/resources/night_music.wav");
	public static final Sound menu_move = new Sound("/com/yellowtail/resources/menu_move.wav");
	public static final Sound menu_select = new Sound("/com/yellowtail/resources/menu_select.wav");
	public static final Sound player_step = new Sound("/com/yellowtail/resources/player_step.wav");
	public static final Sound player_swim = new Sound("/com/yellowtail/resources/player_swim.wav");
	public static final Sound zombie_step = new Sound("/com/yellowtail/resources/zombie_step.wav");

	private AudioClip clip;

	private Sound(String name) {
		try {
			clip = Applet.newAudioClip(Sound.class.getResource(name));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void play() {
		try {
			new Thread() {
				public void run() {
					clip.play();
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public void playContinously()
	{
		try {
			new Thread() {
				public void run() {
					clip.loop();
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	public void playEnd()
	{
		try {
			new Thread() {
				public void run() {
					clip.stop();
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
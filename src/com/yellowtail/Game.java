package com.yellowtail;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.yellowtail.level.MainMenu;
import com.yellowtail.level.World;
import com.yellowtail.world.Sprite;

public class Game extends Canvas implements Runnable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7681062606663127429L;
	private boolean _running;
	private String _activeLevel;
	private Map<String, Level> _levels;
	private BufferStrategy _bufferStrategy;
	private Boolean _loading;
	private int _pauseInterval;
	private ArrayList<Integer> _frameTimes;
	private int _fps;
	public static String NAME = "YellowTail";
		
	public Game()
	{
		_levels = new HashMap<String, Level>();
		_frameTimes = new ArrayList<>();
		_loading = true;
	}
	
	private void init()
	{
		System.setProperty("sun.java2d.opengl", "True");
		this.requestFocus();
		this.createBufferStrategy(3);
		this._bufferStrategy = this.getBufferStrategy();
		this._levels.put("MainMenu", new MainMenu(this));
		this._levels.put("World", new World(this));
		InputHandler.inputWatcher.attachKeyListener(this);
		InputHandler.inputWatcher.SetCommand(Command.Up, KeyEvent.VK_W);
		InputHandler.inputWatcher.SetCommand(Command.Left, KeyEvent.VK_A);
		InputHandler.inputWatcher.SetCommand(Command.Down, KeyEvent.VK_S);
		InputHandler.inputWatcher.SetCommand(Command.Right, KeyEvent.VK_D);
		InputHandler.inputWatcher.SetCommand(Command.Select, KeyEvent.VK_J);
		InputHandler.inputWatcher.SetCommand(Command.Attack, KeyEvent.VK_J);
		InputHandler.inputWatcher.SetCommand(Command.ZoomIn, KeyEvent.VK_Q);
		InputHandler.inputWatcher.SetCommand(Command.ZoomOut, KeyEvent.VK_E);
		InputHandler.inputWatcher.SetCommand(Command.Menu, KeyEvent.VK_ESCAPE);
		loadSprites();
		this._activeLevel = "MainMenu";
		this._levels.get(_activeLevel).load();
		this._loading = false;
	}
	
	public void run()
	{
		init();
		while(this._running)
		{
			Time.GameTime.setStartFrame();
			InputHandler.inputWatcher.processCommands();
			drawFrame();
			_pauseInterval = (int) (16 - Time.GameTime.getSinceFrameStart());
			if(_pauseInterval < 0){ _pauseInterval = 0; }
			try {
				Thread.sleep(_pauseInterval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Time.GameTime.setStopFrame();
		}
	}
	
	public void start() {
		this._running = true;
		Thread thr = new Thread(this);
		thr.start();
		try {
			thr.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void stop() {
		this._running = false;
	}
	
	public void Load(String level)
	{
		this._loading = true;
		this._levels.get(this._activeLevel).unload();
		this._activeLevel = level;
		this._levels.get(this._activeLevel).load();
		this._loading = false;
	}
	
	public void NextLevel()
	{
		
	}
	
	public void drawFrame()
	{
		if(!_bufferStrategy.contentsLost() && !this._loading)
		{
			Graphics graphics = _bufferStrategy.getDrawGraphics();
			graphics.clearRect(0, 0, this.getWidth(), this.getHeight());
			this._levels.get(this._activeLevel).drawFrame(graphics, new Point(this.getWidth(), this.getHeight()));
			graphics.setColor(Color.red);
			graphics.setFont(new Font("Console",Font.BOLD, 14));
			_frameTimes.add(Time.GameTime.getLastFrameTime());
			if(_frameTimes.size() > 5){ _frameTimes.remove(0); }
			_fps = 0;
			for(int i=0;i<_frameTimes.size();i++)
			{
				_fps += _frameTimes.get(i);
			}
			if(Time.GameTime.getLastFrameTime() > 0)
			{ graphics.drawString("FPS: "+(1000/(_fps/_frameTimes.size())), 0, 20); }
			else { graphics.drawString("FPS: 1000", 0, 20);	}
			graphics.dispose();
			_bufferStrategy.show();
		}
	}
	
	private void loadSprites()
	{
		try {
			Image SpriteImage = ImageIO.read(this.getClass().getResource("/com/yellowtail/resources/Spritez.png"));
			Sprite.SolidGrass.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));//SolidGrass
			Graphics2D graph = Sprite.SolidGrass.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.SolidGrass.getImage().getWidth(), Sprite.SolidGrass.getImage().getHeight(),0,0,24,24,null);
			
			Sprite.Dirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));//SolidDirt
			graph = Sprite.Dirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.Dirt.getImage().getWidth(), Sprite.Dirt.getImage().getHeight(),0,25,24,49,null);
			
			Sprite.SolidRock.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));//Rock
			graph = Sprite.SolidRock.getImage().createGraphics();
			graph.setColor(Color.gray);
			graph.fillRect(0, 0, Sprite.SolidRock.getImage().getWidth(), Sprite.SolidRock.getImage().getHeight());
			
			Sprite.SolidWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.SolidWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.SolidWater.getImage().getWidth(), Sprite.SolidWater.getImage().getHeight(),100,50,124,74,null);
			
			Sprite.TLGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TLGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TLGrassDirt.getImage().getWidth(), Sprite.TLGrassDirt.getImage().getHeight(),25,0,49,24,null);
			
			Sprite.TGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TGrassDirt.getImage().getWidth(), Sprite.TGrassDirt.getImage().getHeight(),50,0,74,24,null);
			
			Sprite.TRGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TRGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TRGrassDirt.getImage().getWidth(), Sprite.TRGrassDirt.getImage().getHeight(),75,0,99,24,null);
			
			Sprite.BLGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BLGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BLGrassDirt.getImage().getWidth(), Sprite.BLGrassDirt.getImage().getHeight(),25,25,49,49,null);
			
			Sprite.BGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BGrassDirt.getImage().getWidth(), Sprite.BGrassDirt.getImage().getHeight(),50,25,74,49,null);
			
			Sprite.BRGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BRGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BRGrassDirt.getImage().getWidth(), Sprite.BRGrassDirt.getImage().getHeight(),75,25,99,49,null);
			
			Sprite.LGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.LGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.LGrassDirt.getImage().getWidth(), Sprite.LGrassDirt.getImage().getHeight(),50,50,74,74,null);
			
			Sprite.RGrassDirt.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.RGrassDirt.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.RGrassDirt.getImage().getWidth(), Sprite.RGrassDirt.getImage().getHeight(),75,50,99,74,null);
			
			Sprite.TLGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TLGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TLGrassWater.getImage().getWidth(), Sprite.TLGrassWater.getImage().getHeight(),100,0,124,24,null);
			
			Sprite.TGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TGrassWater.getImage().getWidth(), Sprite.TGrassWater.getImage().getHeight(),125,0,149,24,null);
			
			Sprite.TRGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TRGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TRGrassWater.getImage().getWidth(), Sprite.TRGrassWater.getImage().getHeight(),150,0,174,24,null);
			
			Sprite.BLGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BLGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BLGrassWater.getImage().getWidth(), Sprite.BLGrassWater.getImage().getHeight(),100,25,124,49,null);
			
			Sprite.BGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BGrassWater.getImage().getWidth(), Sprite.BGrassWater.getImage().getHeight(),125,25,149,49,null);
			
			Sprite.BRGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BRGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BRGrassWater.getImage().getWidth(), Sprite.BRGrassWater.getImage().getHeight(),150,25,174,49,null);
			
			Sprite.LGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.LGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.LGrassWater.getImage().getWidth(), Sprite.LGrassWater.getImage().getHeight(),125,50,149,74,null);
			
			Sprite.RGrassWater.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.RGrassWater.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.RGrassWater.getImage().getWidth(), Sprite.RGrassWater.getImage().getHeight(),150,50,174,74,null);
			
			Sprite.SolidSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.SolidSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.SolidSand.getImage().getWidth(), Sprite.SolidSand.getImage().getHeight(),175,50,199,74,null);
			
			Sprite.TLWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TLWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TLWaterSand.getImage().getWidth(), Sprite.TLWaterSand.getImage().getHeight(),175,0,199,24,null);
			
			Sprite.TWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TWaterSand.getImage().getWidth(), Sprite.TWaterSand.getImage().getHeight(),200,0,224,24,null);
			
			Sprite.TRWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.TRWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.TRWaterSand.getImage().getWidth(), Sprite.TRWaterSand.getImage().getHeight(),225,0,249,24,null);
			
			Sprite.BLWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BLWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BLWaterSand.getImage().getWidth(), Sprite.BLWaterSand.getImage().getHeight(),175,25,199,49,null);
			
			Sprite.BWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BWaterSand.getImage().getWidth(), Sprite.BWaterSand.getImage().getHeight(),200,25,224,49,null);
			
			Sprite.BRWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.BRWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.BRWaterSand.getImage().getWidth(), Sprite.BRWaterSand.getImage().getHeight(),225,25,249,49,null);
			
			Sprite.LWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.LWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.LWaterSand.getImage().getWidth(), Sprite.LWaterSand.getImage().getHeight(),200,50,224,74,null);
			
			Sprite.RWaterSand.setImage(new BufferedImage(24, 24, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.RWaterSand.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.RWaterSand.getImage().getWidth(), Sprite.RWaterSand.getImage().getHeight(),225,50,249,74,null);
	
			Sprite.FMainFront.setImage(new BufferedImage(22, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainFront.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainFront.getImage().getWidth(), Sprite.FMainFront.getImage().getHeight(),1,96,23,144,null);
			
			Sprite.FMainFrontRH.setImage(new BufferedImage(22, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainFrontRH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainFrontRH.getImage().getWidth(), Sprite.FMainFrontRH.getImage().getHeight(),24,96,46,144,null);
			
			Sprite.FMainFrontLH.setImage(new BufferedImage(22, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainFrontLH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainFrontLH.getImage().getWidth(), Sprite.FMainFrontLH.getImage().getHeight(),47,96,69,144,null);

			Sprite.FMainRSide.setImage(new BufferedImage(24, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainRSide.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainRSide.getImage().getWidth(), Sprite.FMainRSide.getImage().getHeight(),70,96,94,144,null);

			Sprite.FMainRSideRH.setImage(new BufferedImage(24, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainRSideRH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainRSideRH.getImage().getWidth(), Sprite.FMainRSideRH.getImage().getHeight(),95,96,119,144,null);

			Sprite.FMainRSideLH.setImage(new BufferedImage(24, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainRSideLH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainRSideLH.getImage().getWidth(), Sprite.FMainRSideLH.getImage().getHeight(),120,96,144,144,null);

			Sprite.FMainLSide.setImage(new BufferedImage(24, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainLSide.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainLSide.getImage().getWidth(), Sprite.FMainLSide.getImage().getHeight(),145,96,169,144,null);

			Sprite.FMainLSideRH.setImage(new BufferedImage(24, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainLSideRH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainLSideRH.getImage().getWidth(), Sprite.FMainLSideRH.getImage().getHeight(),170,96,194,144,null);

			Sprite.FMainLSideLH.setImage(new BufferedImage(24, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainLSideLH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainLSideLH.getImage().getWidth(), Sprite.FMainLSideLH.getImage().getHeight(),195,96,219,144,null);
			
			Sprite.FMainBack.setImage(new BufferedImage(22, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainBack.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainBack.getImage().getWidth(), Sprite.FMainBack.getImage().getHeight(),220,96,242,144,null);

			Sprite.FMainBackRH.setImage(new BufferedImage(22, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainBackRH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainBackRH.getImage().getWidth(), Sprite.FMainBackRH.getImage().getHeight(),243,96,265,144,null);

			Sprite.FMainBackLH.setImage(new BufferedImage(22, 48, BufferedImage.TYPE_INT_ARGB));
			graph = Sprite.FMainBackLH.getImage().createGraphics();
			graph.drawImage(SpriteImage, 0, 0, Sprite.FMainBackLH.getImage().getWidth(), Sprite.FMainBackLH.getImage().getHeight(),266,96,288,144,null);

			graph.dispose();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}		
	}
}

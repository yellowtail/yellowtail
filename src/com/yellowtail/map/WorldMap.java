/*
 * TODO: Seed is currently hard coded, change to random unless loading map from save.
 * 		 Need to cache map chunks to save on performance
 */

package com.yellowtail.map;

import java.awt.Point;
import com.yellowtail.Map;
import com.yellowtail.utils.Noise;
import com.yellowtail.world.RenderSet;
import com.yellowtail.world.RenderSetRequest;
import com.yellowtail.world.Sprite;

public class WorldMap implements Map 
{
	
	private Noise _noise;
	private int _seed;
	private float _waterLevel;
	private Point _tileResolution;
	private TileSet _cache;
	
	public WorldMap()
	{
		_seed = 4096;
		_waterLevel = 0.1f;
		_noise = new Noise(_seed);
		_tileResolution = new Point(240, 240);
		_cache = new TileSet();
	}
	
	@Override
	public RenderSet getRenderSet(RenderSetRequest request) 
	{
		RenderSet _renderSet = new RenderSet(_tileResolution);
		Point pos = new Point(0, 0);
		Point scaledLowRange = new Point((int)Math.round((request.getCenterPosition().x - request.getRange() / 2) / (double)_tileResolution.x),(int)Math.round((request.getCenterPosition().y - request.getRange() / 2) / (double)_tileResolution.y));
		Point scaledHighRange = new Point((int)Math.round((request.getCenterPosition().x + request.getRange() / 2) / (double)_tileResolution.x),(int)Math.round((request.getCenterPosition().y + request.getRange() / 2) / (double)_tileResolution.y));
		for(int x = scaledLowRange.x - 2; x <= scaledHighRange.x + 1; x++)
		{
			for(int y = scaledLowRange.y - 2; y <= scaledHighRange.y + 1; y++)
			{
				pos.setLocation(x, y);
				if(!_cache.containsTile(pos))
				{
					_cache.addTile(new TileCode((Point)pos.clone(), _noise.noise2(x*0.03f, y*0.03f)));
				}
			}
		}
		for(int x = scaledLowRange.x - 1; x <= scaledHighRange.x; x++)
		{
			for(int y = scaledLowRange.y - 1; y <= scaledHighRange.y; y++)
			{
				pos = new Point(x, y);
				if(!_cache.getTile(pos).isSpriteAssigned())
				{
					float cpos = _cache.getTile(pos).getNoise();
					Point tempPos = new Point(pos.x-1, pos.y);
					float lpos = _cache.getTile(tempPos).getNoise();
					tempPos.setLocation(pos.x,pos.y+1);
					float tpos = _cache.getTile(tempPos).getNoise();
					tempPos.setLocation(pos.x+1,pos.y);
					float rpos = _cache.getTile(tempPos).getNoise();
					tempPos.setLocation(pos.x,pos.y-1);
					float bpos = _cache.getTile(tempPos).getNoise();
	
					if(cpos <= _waterLevel )
					{
						_cache.getTile(pos).setSprite(Sprite.SolidWater);
					}
					else if (tpos <= _waterLevel && lpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.BLWaterSand);
					}
					else if (tpos <= _waterLevel && rpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.BRWaterSand);
					}
					else if (tpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.BWaterSand);
					}
					else if (bpos <= _waterLevel && lpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.TLWaterSand);
					}
					else if (bpos <= _waterLevel && rpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.TRWaterSand);
					}
					else if (bpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.TWaterSand);
					}
					else if (lpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.LWaterSand);
					}
					else if (rpos <= _waterLevel) 
					{
						_cache.getTile(pos).setSprite(Sprite.RWaterSand);
					}
					else
					{
						_cache.getTile(pos).setSprite(Sprite.SolidGrass);
					}
				}
				_renderSet.setTile(_cache.getTranslatedTile(_tileResolution, pos));
			}
		}
		return _renderSet;
	}

	@Override
	public Point getTileResolution()
	{
		return this._tileResolution;
	}

}

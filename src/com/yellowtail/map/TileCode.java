package com.yellowtail.map;

import java.awt.Point;
import java.awt.geom.Point2D;

import com.yellowtail.world.Sprite;


public class TileCode 
{
	private Point _coordinates;
	private Sprite _sprite = null;
	private float _noise;
	private Point2D _tileResolution;
	
	public TileCode(Point coordinates, float noise)
	{
		this._coordinates = coordinates;
		this._noise = noise;
	}
	
	public TileCode(Point coordinates, Point2D tileResolution, Sprite sprite)
	{
		this._coordinates = coordinates;
		this._tileResolution = tileResolution;
		this._sprite = sprite;
	}
	
	public TileCode(Point coordinates, float noise, Point2D tileResolution, Sprite sprite)
	{
		this._coordinates = coordinates;
		this._noise = noise;
		this._tileResolution = tileResolution;
		this._sprite = sprite;
	}
	
	public Point getCoordinates()
	{
		return _coordinates;
	}
	
	public Sprite getSprite()
	{
		return _sprite;
	}
	
	public float getNoise()
	{
		return _noise;
	}
	
	public Point2D getTileResolution()
	{
		return _tileResolution;
	}
	
	public void setSprite(Sprite sprite)
	{
		this._sprite = sprite;
	}
	
	public boolean isSpriteAssigned()
	{
		return (_sprite != null);
	}
}

package com.yellowtail.map;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.HashMap;




public class TileSet 
{
	private HashMap<Point, TileCode> _set;
	
	public TileSet()
	{
		_set = new HashMap<Point, TileCode>();
	}
	
	public void addTile(TileCode tile)
	{
		_set.put(tile.getCoordinates(), tile);
	}
	
	public boolean containsTile(Point coordinates)
	{
		return _set.containsKey(coordinates);
	}
	
	public TileCode getTile(Point coordinates)
	{
		return _set.get(coordinates);
	}
	
	public TileCode getTranslatedTile(Point2D tileResolution,Point coordinates)
	{
		return new TileCode(new Point((int)Math.round(coordinates.x*tileResolution.getX()),(int)Math.round(coordinates.y*tileResolution.getY())), _set.get(coordinates).getNoise(), tileResolution, _set.get(coordinates).getSprite());
	}
}

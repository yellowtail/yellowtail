package com.yellowtail;

import java.awt.Graphics;
import java.awt.Point;

import com.yellowtail.events.CommandEventHandler;


public interface Level extends CommandEventHandler
{
	public void drawFrame(Graphics g, Point size);
	public void load();
	public void unload();
}

package com.yellowtail.world;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import com.yellowtail.map.TileCode;



public class RenderSet 
{
	private ArrayList<TileCode> _tileCodes;
	
	public RenderSet(Point2D tileResolution)
	{
		this._tileCodes = new ArrayList<TileCode>();
	}

	public RenderSet(ArrayList<TileCode> tileCodes)
	{
		this._tileCodes = tileCodes;
	}
	
	public void setTile(TileCode tile)
	{
		this._tileCodes.add(tile);
	}
	
	public ArrayList<TileCode> getTiles()
	{
		return _tileCodes;
	}
	
}

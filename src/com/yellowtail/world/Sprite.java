package com.yellowtail.world;

import java.awt.image.BufferedImage;

public class Sprite 
{
	public static final Sprite SolidGrass = new Sprite(0);
	public static final Sprite Dirt = new Sprite(1);
	public static final Sprite SolidRock = new Sprite(2);
	public static final Sprite SolidWater = new Sprite(3);
	public static final Sprite TLGrassDirt = new Sprite(4);
	public static final Sprite TGrassDirt = new Sprite(5);
	public static final Sprite TRGrassDirt = new Sprite(6);
	public static final Sprite BLGrassDirt = new Sprite(7);
	public static final Sprite BGrassDirt = new Sprite(8);
	public static final Sprite BRGrassDirt = new Sprite(9);
	public static final Sprite LGrassDirt = new Sprite(10);
	public static final Sprite RGrassDirt = new Sprite(11);
	public static final Sprite TLGrassWater = new Sprite(12);
	public static final Sprite TGrassWater = new Sprite(13);
	public static final Sprite TRGrassWater = new Sprite(14);
	public static final Sprite BLGrassWater = new Sprite(15);
	public static final Sprite BGrassWater = new Sprite(16);
	public static final Sprite BRGrassWater = new Sprite(17);
	public static final Sprite LGrassWater = new Sprite(18);
	public static final Sprite RGrassWater = new Sprite(19);
	public static final Sprite SolidSand = new Sprite(20);
	public static final Sprite TLWaterSand = new Sprite(21);
	public static final Sprite TWaterSand = new Sprite(22);
	public static final Sprite TRWaterSand = new Sprite(23);
	public static final Sprite BLWaterSand = new Sprite(24);
	public static final Sprite BWaterSand = new Sprite(25);
	public static final Sprite BRWaterSand = new Sprite(26);
	public static final Sprite LWaterSand = new Sprite(27);
	public static final Sprite RWaterSand = new Sprite(28);
	public static final Sprite FMainFront = new Sprite(29);
	public static final Sprite FMainFrontLH = new Sprite(30);
	public static final Sprite FMainFrontRH = new Sprite(31);
	public static final Sprite FMainLSide = new Sprite(32);
	public static final Sprite FMainLSideLH = new Sprite(33);
	public static final Sprite FMainLSideRH = new Sprite(34);
	public static final Sprite FMainRSide = new Sprite(35);
	public static final Sprite FMainRSideLH = new Sprite(36);
	public static final Sprite FMainRSideRH = new Sprite(37);
	public static final Sprite FMainBack = new Sprite(38);
	public static final Sprite FMainBackRH = new Sprite(39);
	public static final Sprite FMainBackLH = new Sprite(40);
	
	private int _id;
	private BufferedImage _image;
	
	public Sprite(int id)
	{
		_id = id;
	}
	
	public int getID()
	{
		return _id;
	}
	
	public void setImage(BufferedImage image)
	{
		this._image = image;
	}
	
	public BufferedImage getImage()
	{
		return _image;
	}
}

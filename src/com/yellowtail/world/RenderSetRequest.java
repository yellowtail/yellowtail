package com.yellowtail.world;

import java.awt.Point;

public class RenderSetRequest 
{
	private Point _centerPosition;
	private int _range;
	
	public RenderSetRequest(Point centerPosition, int range)
	{
		this._centerPosition = centerPosition;
		this._range = range;
	}
	
	public Point getCenterPosition()
	{
		return this._centerPosition;
	}
	
	public int getRange()
	{
		return this._range;
	}
}

package com.yellowtail.menu;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import com.yellowtail.Sound;
import com.yellowtail.screen.GUIElement;
import com.yellowtail.screen.Layer;
import com.yellowtail.screen.Style;
import com.yellowtail.screen.Text;

public class Menu implements GUIElement {

	private String _name;
	private String _title;
	private Point _position;
	private ArrayList<MenuItem> _menuItems;
	private int _focus = -1;
	private Text titleElement;
	private Layer _layer;
	private Style _itemStyle;
	private Style _selectedItemStyle;
	private int _width;
	private int _height;
	
	public Menu(String name, String title, Point position, Style titleStyle, Style selectedItemStyle, Style itemStyle, Layer layer)
	{
		this._name = name;
		this._title = title;
		this._position = position;
		this._menuItems = new ArrayList<MenuItem>();
		this._layer = layer;
		this.titleElement = new Text("title", new Point(_position.x, _position.y+titleStyle.getFont().getSize()), this._title, titleStyle, layer);
		this._itemStyle = itemStyle;
		this._selectedItemStyle = selectedItemStyle;
		this._width = titleElement.getText().length()*(titleStyle.getFont().getSize()/2);
		this._height = titleStyle.getFont().getSize();
	}
	
	@Override
	public String getName()
	{
		return _name;
	}
	
	public void SelectNext()
	{
		if(_menuItems.size() > 1 && _menuItems.size() - 1 > _focus)
		{
			Sound.menu_move.play();
			_menuItems.get(_focus).Deselect();
			_focus++;
			_menuItems.get(_focus).Select();
		}
	}
	
	public void SelectPrev()
	{
		if(_menuItems.size() > -1 && _focus > 0)
		{
			Sound.menu_move.play();
			_menuItems.get(_focus).Deselect();
			_focus--;
			_menuItems.get(_focus).Select();
		}
	}
	
	public MenuItem getSelected()
	{
		Sound.menu_select.play();
		return _menuItems.get(_focus);
	}
	
	@Override
	public void overlay(Graphics g, Point size) 
	{
		g.setColor(titleElement.getStyle().getBGColor());
		g.fillRect(_position.x, _position.y, _width*2, _height);
		for(int i = 0; i < _menuItems.size(); i++)
		{
			_menuItems.get(i).overlay(g,size);
		}
		this.titleElement.overlay(g,size);
	}

	public void AddMenuItem(String menuItem) 
	{
		MenuItem element = new MenuItem(menuItem, menuItem, _itemStyle, _selectedItemStyle, _layer);
		this._height += element.getStyle().getFont().getSize();
		if(element.getText().length() > _width)
		{
			_width = element.getText().length()*(element.getStyle().getFont().getSize()/2);
		}
		element.setPosition(new Point(_position.x+10,_position.y+_height));
		if(_focus == -1)
		{
			element.Select();
			_focus = 0;
		}
		_menuItems.add(element);
	}

	public void RemoveMenuItem(MenuItem element) 
	{
		_menuItems.remove(element);
		if(_menuItems.size() - 1 > _focus)
		{
			_focus = _menuItems.size() - 1;
		}
		_height = titleElement.getStyle().getFont().getSize();
		_width = titleElement.getText().length();
		for(int i=0;i<_menuItems.size();i++)
		{
			if(_menuItems.get(i).getText().length() > _width)
			{
				_width = _menuItems.get(i).getText().length()*(_menuItems.get(i).getStyle().getFont().getSize()/2);
			}
			_height += _menuItems.get(i).getStyle().getFont().getSize();
			_menuItems.get(i).setPosition(new Point(_position.x+10, _position.y+_height));
		}
	}

	public int getHeight()
	{
		return _height;
	}
	
	public int getWidth()
	{
		return _width;
	}

	@Override
	public void setPosition(Point position) 
	{
		this.titleElement.setPosition(new Point(position.x,position.y+this.titleElement.getStyle().getFont().getSize()));
		_height = titleElement.getStyle().getFont().getSize();
		_width = titleElement.getText().length();
		for(int i=0;i<_menuItems.size();i++)
		{
			if(_menuItems.get(i).getText().length() > _width)
			{
				_width = _menuItems.get(i).getText().length()*(_menuItems.get(i).getStyle().getFont().getSize()/2);
			}
			_height += _menuItems.get(i).getStyle().getFont().getSize();
			_menuItems.get(i).setPosition(new Point(position.x+10, position.y+_height));
		}
		this._position = position;
	}

	@Override
	public Layer getLayer() 
	{
		return this._layer;
	}
}

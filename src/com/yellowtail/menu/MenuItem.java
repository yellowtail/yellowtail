package com.yellowtail.menu;

import java.awt.Graphics;
import java.awt.Point;

import com.yellowtail.screen.GUIElement;
import com.yellowtail.screen.Layer;
import com.yellowtail.screen.Style;

public class MenuItem implements GUIElement
{
	private String _name;
	private String _text;
	private Style _style;
	private Style _selectedStyle;
	private boolean _selected;
	private Point _position;
	private Layer _layer;
	private int _width;
	private int _height;
	
	public MenuItem(String name, String text, Style style, Style selectedStyle, Layer layer) 
	{
		this._name = name;
		this._text = text;
		this._style = style;
		this._width = text.length()*style.getFont().getSize();
		this._height = style.getFont().getSize();
		this._selectedStyle = selectedStyle;
		this._layer = layer;
	}
	
	public String getText()
	{
		return this._text;
	}
	
	public Style getStyle()
	{
		return this._style;
	}
	
	public Style getSelectedStyle()
	{
		return this._selectedStyle;
	}
	
	public void Deselect()
	{
		this._selected = false;
	}
	
	public void Select()
	{
		this._selected = true;
	}
	
	public boolean isSelected()
	{
		return this._selected;
	}
	
	public int getHeight()
	{
		if(this._selected)
		{
			return this._selectedStyle.getFont().getSize();
		}
		else
		{
			return this._style.getFont().getSize();
		}
	}

	@Override
	public void setPosition(Point position) 
	{
		this._position = position;
	}

	@Override
	public void overlay(Graphics g, Point size) {
		g.setColor(this._style.getBGColor());
		g.fillRect(_position.x, _position.y-_height, _width, _height);
		if(this._selected)
		{
			g.setColor(this._selectedStyle.getColor());
			g.setFont(this._selectedStyle.getFont());
		}
		else
		{
			g.setColor(this._style.getColor());
			g.setFont(this._style.getFont());
		}
		g.drawString(_text, _position.x, _position.y);
	}

	@Override
	public Layer getLayer() 
	{
		return this._layer;
	}

	@Override
	public String getName() 
	{
		return this._name;
	}
}
